package java.io;


import java.lang.reflect.Array;
import java.rmi.RemoteException;

import static java.io.ObjectStreamConstants.TC_OBJECT;

/**
 * @Description:
 * @author: niuyh
 * @date: 2022/9/29
 */
public class ObjectInputStream {
    public boolean readIgnoreCheck = false;

    private static final int NULL_HANDLE = -1;
    private static final Object unsharedMarker = new Object();
    private final ObjectInputStream.BlockDataInputStream bin = null;
    private final ObjectInputStream.HandleTable handles = null;
    private int passHandle = ObjectInputStream.NULL_HANDLE;

    protected ObjectStreamClass readClassDescriptor()
            throws IOException, ClassNotFoundException {
        throw new RuntimeException();
    }

    private Object readOrdinaryObject(boolean unshared)
            throws IOException {
        if (bin.readByte() != TC_OBJECT) {
            throw new InternalError();
        }

        ObjectStreamClass desc = readClassDesc(false);
        if (!readIgnoreCheck) {
            desc.checkDeserialize();
        }
        Class<?> cl = desc.forClass();
        if (cl == String.class || cl == Class.class
                || cl == ObjectStreamClass.class) {
            throw new InvalidClassException("invalid class descriptor");
        }

        Object obj;
        try {
            obj = desc.isInstantiable() ? desc.newInstance() : null;
        } catch (Exception ex) {
            throw (IOException) new InvalidClassException(
                    desc.forClass().getName(),
                    "unable to create instance").initCause(ex);
        }

        passHandle = handles.assign(unshared ? unsharedMarker : obj);
        ClassNotFoundException resolveEx = desc.getResolveException();
        if (resolveEx != null) {
            handles.markException(passHandle, resolveEx);
        }

        if (desc.isExternalizable()) {
            readExternalData((Externalizable) obj, desc);
        } else {
            readSerialData(obj, desc);
        }

        handles.finish(passHandle);

        if (obj != null &&
                handles.lookupException(passHandle) == null &&
                desc.hasReadResolveMethod()) {
            Object rep = desc.invokeReadResolve(obj);
            if (unshared && rep.getClass().isArray()) {
                rep = cloneArray(rep);
            }
            if (rep != obj) {
                // Filter the replacement object
                if (rep != null) {
                    if (rep.getClass().isArray()) {
                        filterCheck(rep.getClass(), Array.getLength(rep));
                    } else {
                        filterCheck(rep.getClass(), -1);
                    }
                }
                handles.setObject(passHandle, obj = rep);
            }
        }

        return obj;
    }

    private void filterCheck(Class<?> clazz, int arrayLength)
            throws InvalidClassException {
    }

    private static Object cloneArray(Object array) {
        throw new RuntimeException();
    }

    private void readSerialData(Object obj, ObjectStreamClass desc)
            throws IOException {
    }

    private void readExternalData(Externalizable obj, ObjectStreamClass desc)
            throws IOException {
    }

    private ObjectStreamClass readClassDesc(boolean unshared)
            throws IOException {
        throw new RemoteException();
    }

    private static class HandleTable {
        void setObject(int handle, Object obj) {
        }

        ClassNotFoundException lookupException(int handle) {
            return null;
        }

        void finish(int handle) {
        }

        void markException(int handle, ClassNotFoundException ex) {
        }

        int assign(Object obj) {
            return 0;
        }
    }

    private class BlockDataInputStream
            extends InputStream implements DataInput {
        @Override
        public void readFully(byte[] b) throws IOException {
            throw new RuntimeException("Not Implemented");
        }

        @Override
        public void readFully(byte[] b, int off, int len) throws IOException {
            throw new RuntimeException("Not Implemented");
        }

        @Override
        public int skipBytes(int n) throws IOException {
            throw new RuntimeException("Not Implemented");
        }

        @Override
        public boolean readBoolean() throws IOException {
            throw new RuntimeException("Not Implemented");
        }

        @Override
        public byte readByte() throws IOException {
            throw new RuntimeException("Not Implemented");
        }

        @Override
        public int readUnsignedByte() throws IOException {
            throw new RuntimeException("Not Implemented");
        }

        @Override
        public short readShort() throws IOException {
            throw new RuntimeException("Not Implemented");
        }

        @Override
        public int readUnsignedShort() throws IOException {
            throw new RuntimeException("Not Implemented");
        }

        @Override
        public char readChar() throws IOException {
            throw new RuntimeException("Not Implemented");
        }

        @Override
        public int readInt() throws IOException {
            throw new RuntimeException("Not Implemented");
        }

        @Override
        public long readLong() throws IOException {
            throw new RuntimeException("Not Implemented");
        }

        @Override
        public float readFloat() throws IOException {
            throw new RuntimeException("Not Implemented");
        }

        @Override
        public double readDouble() throws IOException {
            throw new RuntimeException("Not Implemented");
        }

        @Override
        public String readLine() throws IOException {
            throw new RuntimeException("Not Implemented");
        }

        @Override
        public String readUTF() throws IOException {
            throw new RuntimeException("Not Implemented");
        }

        @Override
        public int read() throws IOException {
            throw new RuntimeException("Not Implemented");
        }
    }
}
