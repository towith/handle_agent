package java.io;


import java.util.HashSet;
import java.util.Set;

public class ObjectOutputStream {
    private static final boolean extendedDebugInfo =
            java.security.AccessController.doPrivileged(
                    new sun.security.action.GetBooleanAction(
                            "sun.io.serialization.extendedDebugInfo")).booleanValue();
    private final BlockDataOutputStream bout;
    private final ReplaceTable subs;
    private final HandleTable handles;

    public final Set<String> excludeSet = new HashSet<>();

    private final DebugTraceInfoStack debugInfoStack;
    private boolean enableReplace;
    private int depth;
    private final boolean enableOverride;

    public ObjectOutputStream(OutputStream out) throws IOException {
        verifySubclass();
        bout = new BlockDataOutputStream(out);
        handles = new HandleTable(10, (float) 3.00);
        subs = new ReplaceTable(10, (float) 3.00);
        enableOverride = false;
        writeStreamHeader();
        bout.setBlockDataMode(true);
        if (extendedDebugInfo) {
            debugInfoStack = new DebugTraceInfoStack();
        } else {
            debugInfoStack = null;
        }
    }

    protected void writeStreamHeader() throws IOException {
    }

    private void verifySubclass() {
    }

    private void writeObject0(Object obj, boolean unshared)
            throws IOException {
        boolean oldMode = bout.setBlockDataMode(false);
        depth++;
        try {
            // handle previously written and non-replaceable objects
            int h;
            if ((obj = subs.lookup(obj)) == null) {
                writeNull();
                return;
            } else if (!unshared && (h = handles.lookup(obj)) != -1) {
                writeHandle(h);
                return;
            } else if (obj instanceof Class) {
                writeClass((Class) obj, unshared);
                return;
            } else if (obj instanceof ObjectStreamClass) {
                writeClassDesc((ObjectStreamClass) obj, unshared);
                return;
            }

            // check for replacement object
            Object orig = obj;
            Class<?> cl = obj.getClass();
            ObjectStreamClass desc;
            for (; ; ) {
                // REMIND: skip this check for strings/arrays?
                Class<?> repCl;
                desc = ObjectStreamClass.lookup(cl, true);
                if (!desc.hasWriteReplaceMethod() ||
                        (obj = desc.invokeWriteReplace(obj)) == null ||
                        (repCl = obj.getClass()) == cl) {
                    break;
                }
                cl = repCl;
            }
            if (enableReplace) {
                Object rep = replaceObject(obj);
                if (rep != obj && rep != null) {
                    cl = rep.getClass();
                    desc = ObjectStreamClass.lookup(cl, true);
                }
                obj = rep;
            }

            // if object replaced, run through original checks a second time
            if (obj != orig) {
                subs.assign(orig, obj);
                if (obj == null) {
                    writeNull();
                    return;
                } else if (!unshared && (h = handles.lookup(obj)) != -1) {
                    writeHandle(h);
                    return;
                } else if (obj instanceof Class) {
                    writeClass((Class) obj, unshared);
                    return;
                } else if (obj instanceof ObjectStreamClass) {
                    writeClassDesc((ObjectStreamClass) obj, unshared);
                    return;
                }
            }

            // remaining cases
            if (obj instanceof String) {
                writeString((String) obj, unshared);
            } else if (cl.isArray()) {
                writeArray(obj, desc, unshared);
            } else if (obj instanceof Enum) {
                writeEnum((Enum<?>) obj, desc, unshared);
            } else if (obj instanceof Serializable ||
                    excludeSet.contains(obj.getClass().getCanonicalName())) {
                writeOrdinaryObject(obj, desc, unshared);
            } else {
                if (extendedDebugInfo) {
                    throw new NotSerializableException(cl.getName() + "\n" + debugInfoStack.toString());
                } else {
                    throw new NotSerializableException(cl.getName());
                }
            }
        } finally {
            depth--;
            bout.setBlockDataMode(oldMode);
        }
    }

    protected Object replaceObject(Object obj) throws IOException {
        return null;
    }

    private void writeOrdinaryObject(Object obj,
                                     ObjectStreamClass desc,
                                     boolean unshared)
            throws IOException {

    }

    private void writeArray(Object array,
                            ObjectStreamClass desc,
                            boolean unshared)
            throws IOException {

    }

    private void writeString(String str, boolean unshared) throws IOException {

    }

    private void writeClassDesc(ObjectStreamClass desc, boolean unshared)
            throws IOException {
    }

    private void writeClass(Class<?> cl, boolean unshared) throws IOException {
    }

    private void writeHandle(int handle) throws IOException {
    }

    private void writeNull() throws IOException {

    }


    private void writeEnum(Enum<?> en,
                           ObjectStreamClass desc,
                           boolean unshared)
            throws IOException {
    }

    static class DebugTraceInfoStack {

    }

    static class HandleTable {
        HandleTable(int initialCapacity, float loadFactor) {
        }

        public int lookup(Object obj) {
            throw new RuntimeException("Not Implemented");
        }
    }

    static class ReplaceTable {
        ReplaceTable(int initialCapacity, float loadFactor) {
        }

        public Object lookup(Object obj) {
            throw new RuntimeException("Not Implemented");
        }

        public void assign(Object orig, Object obj) {
            throw new RuntimeException("Not Implemented");
        }
    }

    static class BlockDataOutputStream {
        BlockDataOutputStream(OutputStream out) {
        }

        public boolean setBlockDataMode(boolean b) {
            throw new RuntimeException("Not Implemented");
        }
    }

}
