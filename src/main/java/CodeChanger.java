import javassist.CannotCompileException;
import javassist.ClassPool;
import javassist.CtClass;
import javassist.NotFoundException;
import jdk.internal.org.objectweb.asm.*;

import java.io.*;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.function.Consumer;

import static jdk.internal.org.objectweb.asm.Opcodes.*;

/**
 * @Description:
 * @author: niuyh
 * @date: 2022/9/28
 */
public class CodeChanger implements Handler {
    static byte[] handle(String className, byte[] classfileBuffer, Class<?> classBeingRedefined) throws IOException {
        ClassPool classPool = new ClassPool(true);
        if (HandleAgent.startWith(className, "com.aspose.cad")) {
            try {
                CtClass ctClass = classPool.makeClass(new ByteArrayInputStream(classfileBuffer));
                CtClass[] interfaces = ctClass.getInterfaces();
                /*
                // classBeingRedefined is null
                Class<?>[] interfaces = classBeingRedefined.getInterfaces();
                */
                boolean exists = false;
                for (CtClass anInterface : interfaces) {
                    String name = anInterface.getName();
                    if (name.equals("java.io.Serializable")) {
                        exists = true;
                    }
                }
                if (!exists) {
                    ctClass.addInterface(classPool.get(Serializable.class.getName()));
                    return ctClass.toBytecode();
                }
            } catch (IOException e) {
                throw new RuntimeException(e);
            } catch (CannotCompileException e) {
                throw new RuntimeException(e);
            } catch (NotFoundException e) {
                throw new RuntimeException(e);
            }
        } else if (HandleAgent.isEquals(className, "java.io.ObjectOutputStream")) {
            return modClass(className, classfileBuffer, (param) -> {
                modObjectOutputStream(param);
            }, false);
        } else if (HandleAgent.isEquals(className, "java.io.ObjectInputStream")) {
            return modClass(className, classfileBuffer, (param -> {
                modObjectInputStream(param);
            }), false);
        }
        return classfileBuffer;
    }

    private static void modObjectInputStream(Param param) {

        FieldVisitor fieldVisitor = param.fieldVisitor;
        MethodVisitor methodVisitor = param.methodVisitor;
        ClassWriter classWriter = param.classWriter;

        {
            fieldVisitor = classWriter.visitField(ACC_PUBLIC, "readIgnoreCheck", "Z", null, null);
            fieldVisitor.visitEnd();
        }
        {
            methodVisitor = classWriter.visitMethod(ACC_PRIVATE,
                    "readOrdinaryObject",
                    "(Z)Ljava/lang/Object;",
                    null,
                    new String[]{"java/io/IOException"});
            methodVisitor.visitCode();
            Label label0 = new Label();
            Label label1 = new Label();
            Label label2 = new Label();
            methodVisitor.visitTryCatchBlock(label0, label1, label2, "java/lang/Exception");
            Label label3 = new Label();
            methodVisitor.visitLabel(label3);
            methodVisitor.visitLineNumber(32, label3);
            methodVisitor.visitVarInsn(ALOAD, 0);
            methodVisitor.visitFieldInsn(GETFIELD,
                    "java/io/ObjectInputStream",
                    "bin",
                    "Ljava/io/ObjectInputStream$BlockDataInputStream;");
            methodVisitor.visitMethodInsn(INVOKEVIRTUAL,
                    "java/io/ObjectInputStream$BlockDataInputStream",
                    "readByte",
                    "()B",
                    false);
            methodVisitor.visitIntInsn(BIPUSH, 115);
            Label label4 = new Label();
            methodVisitor.visitJumpInsn(IF_ICMPEQ, label4);
            Label label5 = new Label();
            methodVisitor.visitLabel(label5);
            methodVisitor.visitLineNumber(33, label5);
            methodVisitor.visitTypeInsn(NEW, "java/lang/InternalError");
            methodVisitor.visitInsn(DUP);
            methodVisitor.visitMethodInsn(INVOKESPECIAL, "java/lang/InternalError", "<init>", "()V", false);
            methodVisitor.visitInsn(ATHROW);
            methodVisitor.visitLabel(label4);
            methodVisitor.visitLineNumber(36, label4);
            methodVisitor.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
            methodVisitor.visitVarInsn(ALOAD, 0);
            methodVisitor.visitInsn(ICONST_0);
            methodVisitor.visitMethodInsn(INVOKESPECIAL,
                    "java/io/ObjectInputStream",
                    "readClassDesc",
                    "(Z)Ljava/io/ObjectStreamClass;",
                    false);
            methodVisitor.visitVarInsn(ASTORE, 2);
            Label label6 = new Label();
            methodVisitor.visitLabel(label6);
            methodVisitor.visitLineNumber(37, label6);
            methodVisitor.visitVarInsn(ALOAD, 0);
            methodVisitor.visitFieldInsn(GETFIELD, "java/io/ObjectInputStream", "readIgnoreCheck", "Z");
            Label label7 = new Label();
            methodVisitor.visitJumpInsn(IFNE, label7);
            Label label8 = new Label();
            methodVisitor.visitLabel(label8);
            methodVisitor.visitLineNumber(38, label8);
            methodVisitor.visitVarInsn(ALOAD, 2);
            methodVisitor.visitMethodInsn(INVOKEVIRTUAL, "java/io/ObjectStreamClass", "checkDeserialize", "()V", false);
            methodVisitor.visitLabel(label7);
            methodVisitor.visitLineNumber(40, label7);
            methodVisitor.visitFrame(Opcodes.F_APPEND, 1, new Object[]{"java/io/ObjectStreamClass"}, 0, null);
            methodVisitor.visitVarInsn(ALOAD, 2);
            methodVisitor.visitMethodInsn(INVOKEVIRTUAL,
                    "java/io/ObjectStreamClass",
                    "forClass",
                    "()Ljava/lang/Class;",
                    false);
            methodVisitor.visitVarInsn(ASTORE, 3);
            Label label9 = new Label();
            methodVisitor.visitLabel(label9);
            methodVisitor.visitLineNumber(41, label9);
            methodVisitor.visitVarInsn(ALOAD, 3);
            methodVisitor.visitLdcInsn(Type.getType("Ljava/lang/String;"));
            Label label10 = new Label();
            methodVisitor.visitJumpInsn(IF_ACMPEQ, label10);
            methodVisitor.visitVarInsn(ALOAD, 3);
            methodVisitor.visitLdcInsn(Type.getType("Ljava/lang/Class;"));
            methodVisitor.visitJumpInsn(IF_ACMPEQ, label10);
            methodVisitor.visitVarInsn(ALOAD, 3);
            methodVisitor.visitLdcInsn(Type.getType("Ljava/io/ObjectStreamClass;"));
            methodVisitor.visitJumpInsn(IF_ACMPNE, label0);
            methodVisitor.visitLabel(label10);
            methodVisitor.visitLineNumber(43, label10);
            methodVisitor.visitFrame(Opcodes.F_APPEND, 1, new Object[]{"java/lang/Class"}, 0, null);
            methodVisitor.visitTypeInsn(NEW, "java/io/InvalidClassException");
            methodVisitor.visitInsn(DUP);
            methodVisitor.visitLdcInsn("invalid class descriptor");
            methodVisitor.visitMethodInsn(INVOKESPECIAL,
                    "java/io/InvalidClassException",
                    "<init>",
                    "(Ljava/lang/String;)V",
                    false);
            methodVisitor.visitInsn(ATHROW);
            methodVisitor.visitLabel(label0);
            methodVisitor.visitLineNumber(48, label0);
            methodVisitor.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
            methodVisitor.visitVarInsn(ALOAD, 2);
            methodVisitor.visitMethodInsn(INVOKEVIRTUAL, "java/io/ObjectStreamClass", "isInstantiable", "()Z", false);
            Label label11 = new Label();
            methodVisitor.visitJumpInsn(IFEQ, label11);
            methodVisitor.visitVarInsn(ALOAD, 2);
            methodVisitor.visitMethodInsn(INVOKEVIRTUAL,
                    "java/io/ObjectStreamClass",
                    "newInstance",
                    "()Ljava/lang/Object;",
                    false);
            Label label12 = new Label();
            methodVisitor.visitJumpInsn(GOTO, label12);
            methodVisitor.visitLabel(label11);
            methodVisitor.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
            methodVisitor.visitInsn(ACONST_NULL);
            methodVisitor.visitLabel(label12);
            methodVisitor.visitFrame(Opcodes.F_SAME1, 0, null, 1, new Object[]{"java/lang/Object"});
            methodVisitor.visitVarInsn(ASTORE, 4);
            methodVisitor.visitLabel(label1);
            methodVisitor.visitLineNumber(53, label1);
            Label label13 = new Label();
            methodVisitor.visitJumpInsn(GOTO, label13);
            methodVisitor.visitLabel(label2);
            methodVisitor.visitLineNumber(49, label2);
            methodVisitor.visitFrame(Opcodes.F_SAME1, 0, null, 1, new Object[]{"java/lang/Exception"});
            methodVisitor.visitVarInsn(ASTORE, 5);
            Label label14 = new Label();
            methodVisitor.visitLabel(label14);
            methodVisitor.visitLineNumber(50, label14);
            methodVisitor.visitTypeInsn(NEW, "java/io/InvalidClassException");
            methodVisitor.visitInsn(DUP);
            methodVisitor.visitVarInsn(ALOAD, 2);
            Label label15 = new Label();
            methodVisitor.visitLabel(label15);
            methodVisitor.visitLineNumber(51, label15);
            methodVisitor.visitMethodInsn(INVOKEVIRTUAL,
                    "java/io/ObjectStreamClass",
                    "forClass",
                    "()Ljava/lang/Class;",
                    false);
            methodVisitor.visitMethodInsn(INVOKEVIRTUAL, "java/lang/Class", "getName", "()Ljava/lang/String;", false);
            methodVisitor.visitLdcInsn("unable to create instance");
            methodVisitor.visitMethodInsn(INVOKESPECIAL,
                    "java/io/InvalidClassException",
                    "<init>",
                    "(Ljava/lang/String;Ljava/lang/String;)V",
                    false);
            methodVisitor.visitVarInsn(ALOAD, 5);
            Label label16 = new Label();
            methodVisitor.visitLabel(label16);
            methodVisitor.visitLineNumber(52, label16);
            methodVisitor.visitMethodInsn(INVOKEVIRTUAL,
                    "java/io/InvalidClassException",
                    "initCause",
                    "(Ljava/lang/Throwable;)Ljava/lang/Throwable;",
                    false);
            methodVisitor.visitTypeInsn(CHECKCAST, "java/io/IOException");
            methodVisitor.visitInsn(ATHROW);
            methodVisitor.visitLabel(label13);
            methodVisitor.visitLineNumber(55, label13);
            methodVisitor.visitFrame(Opcodes.F_APPEND, 1, new Object[]{"java/lang/Object"}, 0, null);
            methodVisitor.visitVarInsn(ALOAD, 0);
            methodVisitor.visitVarInsn(ALOAD, 0);
            methodVisitor.visitFieldInsn(GETFIELD,
                    "java/io/ObjectInputStream",
                    "handles",
                    "Ljava/io/ObjectInputStream$HandleTable;");
            methodVisitor.visitVarInsn(ILOAD, 1);
            Label label17 = new Label();
            methodVisitor.visitJumpInsn(IFEQ, label17);
            methodVisitor.visitFieldInsn(GETSTATIC,
                    "java/io/ObjectInputStream",
                    "unsharedMarker",
                    "Ljava/lang/Object;");
            Label label18 = new Label();
            methodVisitor.visitJumpInsn(GOTO, label18);
            methodVisitor.visitLabel(label17);
            methodVisitor.visitFrame(Opcodes.F_FULL,
                    5,
                    new Object[]{"java/io/ObjectInputStream",
                            Opcodes.INTEGER,
                            "java/io/ObjectStreamClass",
                            "java/lang/Class",
                            "java/lang/Object"},
                    2,
                    new Object[]{"java/io/ObjectInputStream", "java/io/ObjectInputStream$HandleTable"});
            methodVisitor.visitVarInsn(ALOAD, 4);
            methodVisitor.visitLabel(label18);
            methodVisitor.visitFrame(Opcodes.F_FULL,
                    5,
                    new Object[]{"java/io/ObjectInputStream",
                            Opcodes.INTEGER,
                            "java/io/ObjectStreamClass",
                            "java/lang/Class",
                            "java/lang/Object"},
                    3,
                    new Object[]{"java/io/ObjectInputStream",
                            "java/io/ObjectInputStream$HandleTable",
                            "java/lang/Object"});
            methodVisitor.visitMethodInsn(INVOKEVIRTUAL,
                    "java/io/ObjectInputStream$HandleTable",
                    "assign",
                    "(Ljava/lang/Object;)I",
                    false);
            methodVisitor.visitFieldInsn(PUTFIELD, "java/io/ObjectInputStream", "passHandle", "I");
            Label label19 = new Label();
            methodVisitor.visitLabel(label19);
            methodVisitor.visitLineNumber(56, label19);
            methodVisitor.visitVarInsn(ALOAD, 2);
            methodVisitor.visitMethodInsn(INVOKEVIRTUAL,
                    "java/io/ObjectStreamClass",
                    "getResolveException",
                    "()Ljava/lang/ClassNotFoundException;",
                    false);
            methodVisitor.visitVarInsn(ASTORE, 5);
            Label label20 = new Label();
            methodVisitor.visitLabel(label20);
            methodVisitor.visitLineNumber(57, label20);
            methodVisitor.visitVarInsn(ALOAD, 5);
            Label label21 = new Label();
            methodVisitor.visitJumpInsn(IFNULL, label21);
            Label label22 = new Label();
            methodVisitor.visitLabel(label22);
            methodVisitor.visitLineNumber(58, label22);
            methodVisitor.visitVarInsn(ALOAD, 0);
            methodVisitor.visitFieldInsn(GETFIELD,
                    "java/io/ObjectInputStream",
                    "handles",
                    "Ljava/io/ObjectInputStream$HandleTable;");
            methodVisitor.visitVarInsn(ALOAD, 0);
            methodVisitor.visitFieldInsn(GETFIELD, "java/io/ObjectInputStream", "passHandle", "I");
            methodVisitor.visitVarInsn(ALOAD, 5);
            methodVisitor.visitMethodInsn(INVOKEVIRTUAL,
                    "java/io/ObjectInputStream$HandleTable",
                    "markException",
                    "(ILjava/lang/ClassNotFoundException;)V",
                    false);
            methodVisitor.visitLabel(label21);
            methodVisitor.visitLineNumber(61, label21);
            methodVisitor.visitFrame(Opcodes.F_APPEND, 1, new Object[]{"java/lang/ClassNotFoundException"}, 0, null);
            methodVisitor.visitVarInsn(ALOAD, 2);
            methodVisitor.visitMethodInsn(INVOKEVIRTUAL, "java/io/ObjectStreamClass", "isExternalizable", "()Z", false);
            Label label23 = new Label();
            methodVisitor.visitJumpInsn(IFEQ, label23);
            Label label24 = new Label();
            methodVisitor.visitLabel(label24);
            methodVisitor.visitLineNumber(62, label24);
            methodVisitor.visitVarInsn(ALOAD, 0);
            methodVisitor.visitVarInsn(ALOAD, 4);
            methodVisitor.visitTypeInsn(CHECKCAST, "java/io/Externalizable");
            methodVisitor.visitVarInsn(ALOAD, 2);
            methodVisitor.visitMethodInsn(INVOKESPECIAL,
                    "java/io/ObjectInputStream",
                    "readExternalData",
                    "(Ljava/io/Externalizable;Ljava/io/ObjectStreamClass;)V",
                    false);
            Label label25 = new Label();
            methodVisitor.visitJumpInsn(GOTO, label25);
            methodVisitor.visitLabel(label23);
            methodVisitor.visitLineNumber(64, label23);
            methodVisitor.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
            methodVisitor.visitVarInsn(ALOAD, 0);
            methodVisitor.visitVarInsn(ALOAD, 4);
            methodVisitor.visitVarInsn(ALOAD, 2);
            methodVisitor.visitMethodInsn(INVOKESPECIAL,
                    "java/io/ObjectInputStream",
                    "readSerialData",
                    "(Ljava/lang/Object;Ljava/io/ObjectStreamClass;)V",
                    false);
            methodVisitor.visitLabel(label25);
            methodVisitor.visitLineNumber(67, label25);
            methodVisitor.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
            methodVisitor.visitVarInsn(ALOAD, 0);
            methodVisitor.visitFieldInsn(GETFIELD,
                    "java/io/ObjectInputStream",
                    "handles",
                    "Ljava/io/ObjectInputStream$HandleTable;");
            methodVisitor.visitVarInsn(ALOAD, 0);
            methodVisitor.visitFieldInsn(GETFIELD, "java/io/ObjectInputStream", "passHandle", "I");
            methodVisitor.visitMethodInsn(INVOKEVIRTUAL,
                    "java/io/ObjectInputStream$HandleTable",
                    "finish",
                    "(I)V",
                    false);
            Label label26 = new Label();
            methodVisitor.visitLabel(label26);
            methodVisitor.visitLineNumber(69, label26);
            methodVisitor.visitVarInsn(ALOAD, 4);
            Label label27 = new Label();
            methodVisitor.visitJumpInsn(IFNULL, label27);
            methodVisitor.visitVarInsn(ALOAD, 0);
            methodVisitor.visitFieldInsn(GETFIELD,
                    "java/io/ObjectInputStream",
                    "handles",
                    "Ljava/io/ObjectInputStream$HandleTable;");
            methodVisitor.visitVarInsn(ALOAD, 0);
            methodVisitor.visitFieldInsn(GETFIELD, "java/io/ObjectInputStream", "passHandle", "I");
            Label label28 = new Label();
            methodVisitor.visitLabel(label28);
            methodVisitor.visitLineNumber(70, label28);
            methodVisitor.visitMethodInsn(INVOKEVIRTUAL,
                    "java/io/ObjectInputStream$HandleTable",
                    "lookupException",
                    "(I)Ljava/lang/ClassNotFoundException;",
                    false);
            methodVisitor.visitJumpInsn(IFNONNULL, label27);
            methodVisitor.visitVarInsn(ALOAD, 2);
            Label label29 = new Label();
            methodVisitor.visitLabel(label29);
            methodVisitor.visitLineNumber(71, label29);
            methodVisitor.visitMethodInsn(INVOKEVIRTUAL,
                    "java/io/ObjectStreamClass",
                    "hasReadResolveMethod",
                    "()Z",
                    false);
            methodVisitor.visitJumpInsn(IFEQ, label27);
            Label label30 = new Label();
            methodVisitor.visitLabel(label30);
            methodVisitor.visitLineNumber(72, label30);
            methodVisitor.visitVarInsn(ALOAD, 2);
            methodVisitor.visitVarInsn(ALOAD, 4);
            methodVisitor.visitMethodInsn(INVOKEVIRTUAL,
                    "java/io/ObjectStreamClass",
                    "invokeReadResolve",
                    "(Ljava/lang/Object;)Ljava/lang/Object;",
                    false);
            methodVisitor.visitVarInsn(ASTORE, 6);
            Label label31 = new Label();
            methodVisitor.visitLabel(label31);
            methodVisitor.visitLineNumber(73, label31);
            methodVisitor.visitVarInsn(ILOAD, 1);
            Label label32 = new Label();
            methodVisitor.visitJumpInsn(IFEQ, label32);
            methodVisitor.visitVarInsn(ALOAD, 6);
            methodVisitor.visitMethodInsn(INVOKEVIRTUAL, "java/lang/Object", "getClass", "()Ljava/lang/Class;", false);
            methodVisitor.visitMethodInsn(INVOKEVIRTUAL, "java/lang/Class", "isArray", "()Z", false);
            methodVisitor.visitJumpInsn(IFEQ, label32);
            Label label33 = new Label();
            methodVisitor.visitLabel(label33);
            methodVisitor.visitLineNumber(74, label33);
            methodVisitor.visitVarInsn(ALOAD, 6);
            methodVisitor.visitMethodInsn(INVOKESTATIC,
                    "java/io/ObjectInputStream",
                    "cloneArray",
                    "(Ljava/lang/Object;)Ljava/lang/Object;",
                    false);
            methodVisitor.visitVarInsn(ASTORE, 6);
            methodVisitor.visitLabel(label32);
            methodVisitor.visitLineNumber(76, label32);
            methodVisitor.visitFrame(Opcodes.F_APPEND, 1, new Object[]{"java/lang/Object"}, 0, null);
            methodVisitor.visitVarInsn(ALOAD, 6);
            methodVisitor.visitVarInsn(ALOAD, 4);
            methodVisitor.visitJumpInsn(IF_ACMPEQ, label27);
            Label label34 = new Label();
            methodVisitor.visitLabel(label34);
            methodVisitor.visitLineNumber(78, label34);
            methodVisitor.visitVarInsn(ALOAD, 6);
            Label label35 = new Label();
            methodVisitor.visitJumpInsn(IFNULL, label35);
            Label label36 = new Label();
            methodVisitor.visitLabel(label36);
            methodVisitor.visitLineNumber(79, label36);
            methodVisitor.visitVarInsn(ALOAD, 6);
            methodVisitor.visitMethodInsn(INVOKEVIRTUAL, "java/lang/Object", "getClass", "()Ljava/lang/Class;", false);
            methodVisitor.visitMethodInsn(INVOKEVIRTUAL, "java/lang/Class", "isArray", "()Z", false);
            Label label37 = new Label();
            methodVisitor.visitJumpInsn(IFEQ, label37);
            Label label38 = new Label();
            methodVisitor.visitLabel(label38);
            methodVisitor.visitLineNumber(80, label38);
            methodVisitor.visitVarInsn(ALOAD, 0);
            methodVisitor.visitVarInsn(ALOAD, 6);
            methodVisitor.visitMethodInsn(INVOKEVIRTUAL, "java/lang/Object", "getClass", "()Ljava/lang/Class;", false);
            methodVisitor.visitVarInsn(ALOAD, 6);
            methodVisitor.visitMethodInsn(INVOKESTATIC,
                    "java/lang/reflect/Array",
                    "getLength",
                    "(Ljava/lang/Object;)I",
                    false);
            methodVisitor.visitMethodInsn(INVOKESPECIAL,
                    "java/io/ObjectInputStream",
                    "filterCheck",
                    "(Ljava/lang/Class;I)V",
                    false);
            methodVisitor.visitJumpInsn(GOTO, label35);
            methodVisitor.visitLabel(label37);
            methodVisitor.visitLineNumber(82, label37);
            methodVisitor.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
            methodVisitor.visitVarInsn(ALOAD, 0);
            methodVisitor.visitVarInsn(ALOAD, 6);
            methodVisitor.visitMethodInsn(INVOKEVIRTUAL, "java/lang/Object", "getClass", "()Ljava/lang/Class;", false);
            methodVisitor.visitInsn(ICONST_M1);
            methodVisitor.visitMethodInsn(INVOKESPECIAL,
                    "java/io/ObjectInputStream",
                    "filterCheck",
                    "(Ljava/lang/Class;I)V",
                    false);
            methodVisitor.visitLabel(label35);
            methodVisitor.visitLineNumber(85, label35);
            methodVisitor.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
            methodVisitor.visitVarInsn(ALOAD, 0);
            methodVisitor.visitFieldInsn(GETFIELD,
                    "java/io/ObjectInputStream",
                    "handles",
                    "Ljava/io/ObjectInputStream$HandleTable;");
            methodVisitor.visitVarInsn(ALOAD, 0);
            methodVisitor.visitFieldInsn(GETFIELD, "java/io/ObjectInputStream", "passHandle", "I");
            methodVisitor.visitVarInsn(ALOAD, 6);
            methodVisitor.visitInsn(DUP);
            methodVisitor.visitVarInsn(ASTORE, 4);
            methodVisitor.visitMethodInsn(INVOKEVIRTUAL,
                    "java/io/ObjectInputStream$HandleTable",
                    "setObject",
                    "(ILjava/lang/Object;)V",
                    false);
            methodVisitor.visitLabel(label27);
            methodVisitor.visitLineNumber(89, label27);
            methodVisitor.visitFrame(Opcodes.F_CHOP, 1, null, 0, null);
            methodVisitor.visitVarInsn(ALOAD, 4);
            methodVisitor.visitInsn(ARETURN);
            Label label39 = new Label();
            methodVisitor.visitLabel(label39);
            methodVisitor.visitLocalVariable("obj", "Ljava/lang/Object;", null, label1, label2, 4);
            methodVisitor.visitLocalVariable("ex", "Ljava/lang/Exception;", null, label14, label13, 5);
            methodVisitor.visitLocalVariable("rep", "Ljava/lang/Object;", null, label31, label27, 6);
            methodVisitor.visitLocalVariable("this", "Ljava/io/ObjectInputStream;", null, label3, label39, 0);
            methodVisitor.visitLocalVariable("unshared", "Z", null, label3, label39, 1);
            methodVisitor.visitLocalVariable("desc", "Ljava/io/ObjectStreamClass;", null, label6, label39, 2);
            methodVisitor.visitLocalVariable("cl", "Ljava/lang/Class;", "Ljava/lang/Class<*>;", label9, label39, 3);
            methodVisitor.visitLocalVariable("obj", "Ljava/lang/Object;", null, label13, label39, 4);
            methodVisitor.visitLocalVariable("resolveEx",
                    "Ljava/lang/ClassNotFoundException;",
                    null,
                    label20,
                    label39,
                    5);
            methodVisitor.visitMaxs(4, 7);
            methodVisitor.visitEnd();
        }

    }

    private static void modObjectOutputStream(Param param) {
        FieldVisitor fieldVisitor = param.fieldVisitor;
        MethodVisitor methodVisitor = param.methodVisitor;
        ClassWriter classWriter = param.classWriter;
        {
            fieldVisitor = classWriter.visitField(ACC_PUBLIC | ACC_FINAL,
                    "excludeSet",
                    "Ljava/util/Set;",
                    "Ljava/util/Set<Ljava/lang/String;>;",
                    null);
            fieldVisitor.visitEnd();
        }

        {
            methodVisitor = classWriter.visitMethod(ACC_PUBLIC,
                    "<init>",
                    "(Ljava/io/OutputStream;)V",
                    null,
                    new String[]{"java/io/IOException"});
            methodVisitor.visitCode();
            Label label0 = new Label();
            methodVisitor.visitLabel(label0);
            methodVisitor.visitLineNumber(23, label0);
            methodVisitor.visitVarInsn(ALOAD, 0);
            methodVisitor.visitMethodInsn(INVOKESPECIAL, "java/lang/Object", "<init>", "()V", false);
            Label label1 = new Label();
            methodVisitor.visitLabel(label1);
            methodVisitor.visitLineNumber(16, label1);
            methodVisitor.visitVarInsn(ALOAD, 0);
            methodVisitor.visitTypeInsn(NEW, "java/util/HashSet");
            methodVisitor.visitInsn(DUP);
            methodVisitor.visitMethodInsn(INVOKESPECIAL, "java/util/HashSet", "<init>", "()V", false);
            methodVisitor.visitFieldInsn(PUTFIELD, "java/io/ObjectOutputStream", "excludeSet", "Ljava/util/Set;");
            Label label2 = new Label();
            methodVisitor.visitLabel(label2);
            methodVisitor.visitLineNumber(24, label2);
            methodVisitor.visitVarInsn(ALOAD, 0);
            methodVisitor.visitMethodInsn(INVOKESPECIAL, "java/io/ObjectOutputStream", "verifySubclass", "()V", false);
            Label label3 = new Label();
            methodVisitor.visitLabel(label3);
            methodVisitor.visitLineNumber(25, label3);
            methodVisitor.visitVarInsn(ALOAD, 0);
            methodVisitor.visitTypeInsn(NEW, "java/io/ObjectOutputStream$BlockDataOutputStream");
            methodVisitor.visitInsn(DUP);
            methodVisitor.visitVarInsn(ALOAD, 1);
            methodVisitor.visitMethodInsn(INVOKESPECIAL,
                    "java/io/ObjectOutputStream$BlockDataOutputStream",
                    "<init>",
                    "(Ljava/io/OutputStream;)V",
                    false);
            methodVisitor.visitFieldInsn(PUTFIELD,
                    "java/io/ObjectOutputStream",
                    "bout",
                    "Ljava/io/ObjectOutputStream$BlockDataOutputStream;");
            Label label4 = new Label();
            methodVisitor.visitLabel(label4);
            methodVisitor.visitLineNumber(26, label4);
            methodVisitor.visitVarInsn(ALOAD, 0);
            methodVisitor.visitTypeInsn(NEW, "java/io/ObjectOutputStream$HandleTable");
            methodVisitor.visitInsn(DUP);
            methodVisitor.visitIntInsn(BIPUSH, 10);
            methodVisitor.visitLdcInsn(new Float("3.0"));
            methodVisitor.visitMethodInsn(INVOKESPECIAL,
                    "java/io/ObjectOutputStream$HandleTable",
                    "<init>",
                    "(IF)V",
                    false);
            methodVisitor.visitFieldInsn(PUTFIELD,
                    "java/io/ObjectOutputStream",
                    "handles",
                    "Ljava/io/ObjectOutputStream$HandleTable;");
            Label label5 = new Label();
            methodVisitor.visitLabel(label5);
            methodVisitor.visitLineNumber(27, label5);
            methodVisitor.visitVarInsn(ALOAD, 0);
            methodVisitor.visitTypeInsn(NEW, "java/io/ObjectOutputStream$ReplaceTable");
            methodVisitor.visitInsn(DUP);
            methodVisitor.visitIntInsn(BIPUSH, 10);
            methodVisitor.visitLdcInsn(new Float("3.0"));
            methodVisitor.visitMethodInsn(INVOKESPECIAL,
                    "java/io/ObjectOutputStream$ReplaceTable",
                    "<init>",
                    "(IF)V",
                    false);
            methodVisitor.visitFieldInsn(PUTFIELD,
                    "java/io/ObjectOutputStream",
                    "subs",
                    "Ljava/io/ObjectOutputStream$ReplaceTable;");
            Label label6 = new Label();
            methodVisitor.visitLabel(label6);
            methodVisitor.visitLineNumber(28, label6);
            methodVisitor.visitVarInsn(ALOAD, 0);
            methodVisitor.visitInsn(ICONST_0);
            methodVisitor.visitFieldInsn(PUTFIELD, "java/io/ObjectOutputStream", "enableOverride", "Z");
            Label label7 = new Label();
            methodVisitor.visitLabel(label7);
            methodVisitor.visitLineNumber(29, label7);
            methodVisitor.visitVarInsn(ALOAD, 0);
            methodVisitor.visitMethodInsn(INVOKEVIRTUAL,
                    "java/io/ObjectOutputStream",
                    "writeStreamHeader",
                    "()V",
                    false);
            Label label8 = new Label();
            methodVisitor.visitLabel(label8);
            methodVisitor.visitLineNumber(30, label8);
            methodVisitor.visitVarInsn(ALOAD, 0);
            methodVisitor.visitFieldInsn(GETFIELD,
                    "java/io/ObjectOutputStream",
                    "bout",
                    "Ljava/io/ObjectOutputStream$BlockDataOutputStream;");
            methodVisitor.visitInsn(ICONST_1);
            methodVisitor.visitMethodInsn(INVOKEVIRTUAL,
                    "java/io/ObjectOutputStream$BlockDataOutputStream",
                    "setBlockDataMode",
                    "(Z)Z",
                    false);
            methodVisitor.visitInsn(POP);
            Label label9 = new Label();
            methodVisitor.visitLabel(label9);
            methodVisitor.visitLineNumber(31, label9);
            methodVisitor.visitFieldInsn(GETSTATIC, "java/io/ObjectOutputStream", "extendedDebugInfo", "Z");
            Label label10 = new Label();
            methodVisitor.visitJumpInsn(IFEQ, label10);
            Label label11 = new Label();
            methodVisitor.visitLabel(label11);
            methodVisitor.visitLineNumber(32, label11);
            methodVisitor.visitVarInsn(ALOAD, 0);
            methodVisitor.visitTypeInsn(NEW, "java/io/ObjectOutputStream$DebugTraceInfoStack");
            methodVisitor.visitInsn(DUP);
            methodVisitor.visitMethodInsn(INVOKESPECIAL,
                    "java/io/ObjectOutputStream$DebugTraceInfoStack",
                    "<init>",
                    "()V",
                    false);
            methodVisitor.visitFieldInsn(PUTFIELD,
                    "java/io/ObjectOutputStream",
                    "debugInfoStack",
                    "Ljava/io/ObjectOutputStream$DebugTraceInfoStack;");
            Label label12 = new Label();
            methodVisitor.visitJumpInsn(GOTO, label12);
            methodVisitor.visitLabel(label10);
            methodVisitor.visitLineNumber(34, label10);
            methodVisitor.visitFrame(Opcodes.F_FULL,
                    2,
                    new Object[]{"java/io/ObjectOutputStream", "java/io/OutputStream"},
                    0,
                    new Object[]{});
            methodVisitor.visitVarInsn(ALOAD, 0);
            methodVisitor.visitInsn(ACONST_NULL);
            methodVisitor.visitFieldInsn(PUTFIELD,
                    "java/io/ObjectOutputStream",
                    "debugInfoStack",
                    "Ljava/io/ObjectOutputStream$DebugTraceInfoStack;");
            methodVisitor.visitLabel(label12);
            methodVisitor.visitLineNumber(36, label12);
            methodVisitor.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
            methodVisitor.visitInsn(RETURN);
            Label label13 = new Label();
            methodVisitor.visitLabel(label13);
            methodVisitor.visitLocalVariable("this", "Ljava/io/ObjectOutputStream;", null, label0, label13, 0);
            methodVisitor.visitLocalVariable("out", "Ljava/io/OutputStream;", null, label0, label13, 1);
            methodVisitor.visitMaxs(5, 2);
            methodVisitor.visitEnd();
        }

        {
            methodVisitor = classWriter.visitMethod(ACC_PRIVATE,
                    "writeObject0",
                    "(Ljava/lang/Object;Z)V",
                    null,
                    new String[]{"java/io/IOException"});
            methodVisitor.visitCode();
            Label label0 = new Label();
            Label label1 = new Label();
            Label label2 = new Label();
            methodVisitor.visitTryCatchBlock(label0, label1, label2, null);
            Label label3 = new Label();
            Label label4 = new Label();
            methodVisitor.visitTryCatchBlock(label3, label4, label2, null);
            Label label5 = new Label();
            Label label6 = new Label();
            methodVisitor.visitTryCatchBlock(label5, label6, label2, null);
            Label label7 = new Label();
            Label label8 = new Label();
            methodVisitor.visitTryCatchBlock(label7, label8, label2, null);
            Label label9 = new Label();
            Label label10 = new Label();
            methodVisitor.visitTryCatchBlock(label9, label10, label2, null);
            Label label11 = new Label();
            Label label12 = new Label();
            methodVisitor.visitTryCatchBlock(label11, label12, label2, null);
            Label label13 = new Label();
            Label label14 = new Label();
            methodVisitor.visitTryCatchBlock(label13, label14, label2, null);
            Label label15 = new Label();
            Label label16 = new Label();
            methodVisitor.visitTryCatchBlock(label15, label16, label2, null);
            Label label17 = new Label();
            Label label18 = new Label();
            methodVisitor.visitTryCatchBlock(label17, label18, label2, null);
            Label label19 = new Label();
            methodVisitor.visitTryCatchBlock(label2, label19, label2, null);
            Label label20 = new Label();
            methodVisitor.visitLabel(label20);
            methodVisitor.visitLineNumber(46, label20);
            methodVisitor.visitVarInsn(ALOAD, 0);
            methodVisitor.visitFieldInsn(GETFIELD,
                    "java/io/ObjectOutputStream",
                    "bout",
                    "Ljava/io/ObjectOutputStream$BlockDataOutputStream;");
            methodVisitor.visitInsn(ICONST_0);
            methodVisitor.visitMethodInsn(INVOKEVIRTUAL,
                    "java/io/ObjectOutputStream$BlockDataOutputStream",
                    "setBlockDataMode",
                    "(Z)Z",
                    false);
            methodVisitor.visitVarInsn(ISTORE, 3);
            Label label21 = new Label();
            methodVisitor.visitLabel(label21);
            methodVisitor.visitLineNumber(47, label21);
            methodVisitor.visitVarInsn(ALOAD, 0);
            methodVisitor.visitInsn(DUP);
            methodVisitor.visitFieldInsn(GETFIELD, "java/io/ObjectOutputStream", "depth", "I");
            methodVisitor.visitInsn(ICONST_1);
            methodVisitor.visitInsn(IADD);
            methodVisitor.visitFieldInsn(PUTFIELD, "java/io/ObjectOutputStream", "depth", "I");
            methodVisitor.visitLabel(label0);
            methodVisitor.visitLineNumber(51, label0);
            methodVisitor.visitVarInsn(ALOAD, 0);
            methodVisitor.visitFieldInsn(GETFIELD,
                    "java/io/ObjectOutputStream",
                    "subs",
                    "Ljava/io/ObjectOutputStream$ReplaceTable;");
            methodVisitor.visitVarInsn(ALOAD, 1);
            methodVisitor.visitMethodInsn(INVOKEVIRTUAL,
                    "java/io/ObjectOutputStream$ReplaceTable",
                    "lookup",
                    "(Ljava/lang/Object;)Ljava/lang/Object;",
                    false);
            methodVisitor.visitInsn(DUP);
            methodVisitor.visitVarInsn(ASTORE, 1);
            methodVisitor.visitJumpInsn(IFNONNULL, label3);
            Label label22 = new Label();
            methodVisitor.visitLabel(label22);
            methodVisitor.visitLineNumber(52, label22);
            methodVisitor.visitVarInsn(ALOAD, 0);
            methodVisitor.visitMethodInsn(INVOKESPECIAL, "java/io/ObjectOutputStream", "writeNull", "()V", false);
            methodVisitor.visitLabel(label1);
            methodVisitor.visitLineNumber(125, label1);
            methodVisitor.visitVarInsn(ALOAD, 0);
            methodVisitor.visitInsn(DUP);
            methodVisitor.visitFieldInsn(GETFIELD, "java/io/ObjectOutputStream", "depth", "I");
            methodVisitor.visitInsn(ICONST_1);
            methodVisitor.visitInsn(ISUB);
            methodVisitor.visitFieldInsn(PUTFIELD, "java/io/ObjectOutputStream", "depth", "I");
            Label label23 = new Label();
            methodVisitor.visitLabel(label23);
            methodVisitor.visitLineNumber(126, label23);
            methodVisitor.visitVarInsn(ALOAD, 0);
            methodVisitor.visitFieldInsn(GETFIELD,
                    "java/io/ObjectOutputStream",
                    "bout",
                    "Ljava/io/ObjectOutputStream$BlockDataOutputStream;");
            methodVisitor.visitVarInsn(ILOAD, 3);
            methodVisitor.visitMethodInsn(INVOKEVIRTUAL,
                    "java/io/ObjectOutputStream$BlockDataOutputStream",
                    "setBlockDataMode",
                    "(Z)Z",
                    false);
            methodVisitor.visitInsn(POP);
            Label label24 = new Label();
            methodVisitor.visitLabel(label24);
            methodVisitor.visitLineNumber(53, label24);
            methodVisitor.visitInsn(RETURN);
            methodVisitor.visitLabel(label3);
            methodVisitor.visitLineNumber(54, label3);
            methodVisitor.visitFrame(Opcodes.F_APPEND, 1, new Object[]{Opcodes.INTEGER}, 0, null);
            methodVisitor.visitVarInsn(ILOAD, 2);
            methodVisitor.visitJumpInsn(IFNE, label5);
            methodVisitor.visitVarInsn(ALOAD, 0);
            methodVisitor.visitFieldInsn(GETFIELD,
                    "java/io/ObjectOutputStream",
                    "handles",
                    "Ljava/io/ObjectOutputStream$HandleTable;");
            methodVisitor.visitVarInsn(ALOAD, 1);
            methodVisitor.visitMethodInsn(INVOKEVIRTUAL,
                    "java/io/ObjectOutputStream$HandleTable",
                    "lookup",
                    "(Ljava/lang/Object;)I",
                    false);
            methodVisitor.visitInsn(DUP);
            methodVisitor.visitVarInsn(ISTORE, 4);
            Label label25 = new Label();
            methodVisitor.visitLabel(label25);
            methodVisitor.visitInsn(ICONST_M1);
            methodVisitor.visitJumpInsn(IF_ICMPEQ, label5);
            Label label26 = new Label();
            methodVisitor.visitLabel(label26);
            methodVisitor.visitLineNumber(55, label26);
            methodVisitor.visitVarInsn(ALOAD, 0);
            methodVisitor.visitVarInsn(ILOAD, 4);
            methodVisitor.visitMethodInsn(INVOKESPECIAL, "java/io/ObjectOutputStream", "writeHandle", "(I)V", false);
            methodVisitor.visitLabel(label4);
            methodVisitor.visitLineNumber(125, label4);
            methodVisitor.visitVarInsn(ALOAD, 0);
            methodVisitor.visitInsn(DUP);
            methodVisitor.visitFieldInsn(GETFIELD, "java/io/ObjectOutputStream", "depth", "I");
            methodVisitor.visitInsn(ICONST_1);
            methodVisitor.visitInsn(ISUB);
            methodVisitor.visitFieldInsn(PUTFIELD, "java/io/ObjectOutputStream", "depth", "I");
            Label label27 = new Label();
            methodVisitor.visitLabel(label27);
            methodVisitor.visitLineNumber(126, label27);
            methodVisitor.visitVarInsn(ALOAD, 0);
            methodVisitor.visitFieldInsn(GETFIELD,
                    "java/io/ObjectOutputStream",
                    "bout",
                    "Ljava/io/ObjectOutputStream$BlockDataOutputStream;");
            methodVisitor.visitVarInsn(ILOAD, 3);
            methodVisitor.visitMethodInsn(INVOKEVIRTUAL,
                    "java/io/ObjectOutputStream$BlockDataOutputStream",
                    "setBlockDataMode",
                    "(Z)Z",
                    false);
            methodVisitor.visitInsn(POP);
            Label label28 = new Label();
            methodVisitor.visitLabel(label28);
            methodVisitor.visitLineNumber(56, label28);
            methodVisitor.visitInsn(RETURN);
            methodVisitor.visitLabel(label5);
            methodVisitor.visitLineNumber(57, label5);
            methodVisitor.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
            methodVisitor.visitVarInsn(ALOAD, 1);
            methodVisitor.visitTypeInsn(INSTANCEOF, "java/lang/Class");
            methodVisitor.visitJumpInsn(IFEQ, label7);
            Label label29 = new Label();
            methodVisitor.visitLabel(label29);
            methodVisitor.visitLineNumber(58, label29);
            methodVisitor.visitVarInsn(ALOAD, 0);
            methodVisitor.visitVarInsn(ALOAD, 1);
            methodVisitor.visitTypeInsn(CHECKCAST, "java/lang/Class");
            methodVisitor.visitVarInsn(ILOAD, 2);
            methodVisitor.visitMethodInsn(INVOKESPECIAL,
                    "java/io/ObjectOutputStream",
                    "writeClass",
                    "(Ljava/lang/Class;Z)V",
                    false);
            methodVisitor.visitLabel(label6);
            methodVisitor.visitLineNumber(125, label6);
            methodVisitor.visitVarInsn(ALOAD, 0);
            methodVisitor.visitInsn(DUP);
            methodVisitor.visitFieldInsn(GETFIELD, "java/io/ObjectOutputStream", "depth", "I");
            methodVisitor.visitInsn(ICONST_1);
            methodVisitor.visitInsn(ISUB);
            methodVisitor.visitFieldInsn(PUTFIELD, "java/io/ObjectOutputStream", "depth", "I");
            Label label30 = new Label();
            methodVisitor.visitLabel(label30);
            methodVisitor.visitLineNumber(126, label30);
            methodVisitor.visitVarInsn(ALOAD, 0);
            methodVisitor.visitFieldInsn(GETFIELD,
                    "java/io/ObjectOutputStream",
                    "bout",
                    "Ljava/io/ObjectOutputStream$BlockDataOutputStream;");
            methodVisitor.visitVarInsn(ILOAD, 3);
            methodVisitor.visitMethodInsn(INVOKEVIRTUAL,
                    "java/io/ObjectOutputStream$BlockDataOutputStream",
                    "setBlockDataMode",
                    "(Z)Z",
                    false);
            methodVisitor.visitInsn(POP);
            Label label31 = new Label();
            methodVisitor.visitLabel(label31);
            methodVisitor.visitLineNumber(59, label31);
            methodVisitor.visitInsn(RETURN);
            methodVisitor.visitLabel(label7);
            methodVisitor.visitLineNumber(60, label7);
            methodVisitor.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
            methodVisitor.visitVarInsn(ALOAD, 1);
            methodVisitor.visitTypeInsn(INSTANCEOF, "java/io/ObjectStreamClass");
            methodVisitor.visitJumpInsn(IFEQ, label9);
            Label label32 = new Label();
            methodVisitor.visitLabel(label32);
            methodVisitor.visitLineNumber(61, label32);
            methodVisitor.visitVarInsn(ALOAD, 0);
            methodVisitor.visitVarInsn(ALOAD, 1);
            methodVisitor.visitTypeInsn(CHECKCAST, "java/io/ObjectStreamClass");
            methodVisitor.visitVarInsn(ILOAD, 2);
            methodVisitor.visitMethodInsn(INVOKESPECIAL,
                    "java/io/ObjectOutputStream",
                    "writeClassDesc",
                    "(Ljava/io/ObjectStreamClass;Z)V",
                    false);
            methodVisitor.visitLabel(label8);
            methodVisitor.visitLineNumber(125, label8);
            methodVisitor.visitVarInsn(ALOAD, 0);
            methodVisitor.visitInsn(DUP);
            methodVisitor.visitFieldInsn(GETFIELD, "java/io/ObjectOutputStream", "depth", "I");
            methodVisitor.visitInsn(ICONST_1);
            methodVisitor.visitInsn(ISUB);
            methodVisitor.visitFieldInsn(PUTFIELD, "java/io/ObjectOutputStream", "depth", "I");
            Label label33 = new Label();
            methodVisitor.visitLabel(label33);
            methodVisitor.visitLineNumber(126, label33);
            methodVisitor.visitVarInsn(ALOAD, 0);
            methodVisitor.visitFieldInsn(GETFIELD,
                    "java/io/ObjectOutputStream",
                    "bout",
                    "Ljava/io/ObjectOutputStream$BlockDataOutputStream;");
            methodVisitor.visitVarInsn(ILOAD, 3);
            methodVisitor.visitMethodInsn(INVOKEVIRTUAL,
                    "java/io/ObjectOutputStream$BlockDataOutputStream",
                    "setBlockDataMode",
                    "(Z)Z",
                    false);
            methodVisitor.visitInsn(POP);
            Label label34 = new Label();
            methodVisitor.visitLabel(label34);
            methodVisitor.visitLineNumber(62, label34);
            methodVisitor.visitInsn(RETURN);
            methodVisitor.visitLabel(label9);
            methodVisitor.visitLineNumber(66, label9);
            methodVisitor.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
            methodVisitor.visitVarInsn(ALOAD, 1);
            methodVisitor.visitVarInsn(ASTORE, 5);
            Label label35 = new Label();
            methodVisitor.visitLabel(label35);
            methodVisitor.visitLineNumber(67, label35);
            methodVisitor.visitVarInsn(ALOAD, 1);
            methodVisitor.visitMethodInsn(INVOKEVIRTUAL, "java/lang/Object", "getClass", "()Ljava/lang/Class;", false);
            methodVisitor.visitVarInsn(ASTORE, 6);
            Label label36 = new Label();
            methodVisitor.visitLabel(label36);
            methodVisitor.visitLineNumber(72, label36);
            methodVisitor.visitFrame(Opcodes.F_APPEND,
                    3,
                    new Object[]{Opcodes.TOP, "java/lang/Object", "java/lang/Class"},
                    0,
                    null);
            methodVisitor.visitVarInsn(ALOAD, 6);
            methodVisitor.visitInsn(ICONST_1);
            methodVisitor.visitMethodInsn(INVOKESTATIC,
                    "java/io/ObjectStreamClass",
                    "lookup",
                    "(Ljava/lang/Class;Z)Ljava/io/ObjectStreamClass;",
                    false);
            methodVisitor.visitVarInsn(ASTORE, 7);
            Label label37 = new Label();
            methodVisitor.visitLabel(label37);
            methodVisitor.visitLineNumber(73, label37);
            methodVisitor.visitVarInsn(ALOAD, 7);
            methodVisitor.visitMethodInsn(INVOKEVIRTUAL,
                    "java/io/ObjectStreamClass",
                    "hasWriteReplaceMethod",
                    "()Z",
                    false);
            Label label38 = new Label();
            methodVisitor.visitJumpInsn(IFEQ, label38);
            methodVisitor.visitVarInsn(ALOAD, 7);
            methodVisitor.visitVarInsn(ALOAD, 1);
            Label label39 = new Label();
            methodVisitor.visitLabel(label39);
            methodVisitor.visitLineNumber(74, label39);
            methodVisitor.visitMethodInsn(INVOKEVIRTUAL,
                    "java/io/ObjectStreamClass",
                    "invokeWriteReplace",
                    "(Ljava/lang/Object;)Ljava/lang/Object;",
                    false);
            methodVisitor.visitInsn(DUP);
            methodVisitor.visitVarInsn(ASTORE, 1);
            methodVisitor.visitJumpInsn(IFNULL, label38);
            methodVisitor.visitVarInsn(ALOAD, 1);
            Label label40 = new Label();
            methodVisitor.visitLabel(label40);
            methodVisitor.visitLineNumber(75, label40);
            methodVisitor.visitMethodInsn(INVOKEVIRTUAL, "java/lang/Object", "getClass", "()Ljava/lang/Class;", false);
            methodVisitor.visitInsn(DUP);
            methodVisitor.visitVarInsn(ASTORE, 8);
            Label label41 = new Label();
            methodVisitor.visitLabel(label41);
            methodVisitor.visitVarInsn(ALOAD, 6);
            Label label42 = new Label();
            methodVisitor.visitJumpInsn(IF_ACMPNE, label42);
            Label label43 = new Label();
            methodVisitor.visitLabel(label43);
            methodVisitor.visitLineNumber(76, label43);
            methodVisitor.visitJumpInsn(GOTO, label38);
            methodVisitor.visitLabel(label42);
            methodVisitor.visitLineNumber(78, label42);
            methodVisitor.visitFrame(Opcodes.F_APPEND,
                    2,
                    new Object[]{"java/io/ObjectStreamClass", "java/lang/Class"},
                    0,
                    null);
            methodVisitor.visitVarInsn(ALOAD, 8);
            methodVisitor.visitVarInsn(ASTORE, 6);
            Label label44 = new Label();
            methodVisitor.visitLabel(label44);
            methodVisitor.visitLineNumber(79, label44);
            methodVisitor.visitJumpInsn(GOTO, label36);
            methodVisitor.visitLabel(label38);
            methodVisitor.visitLineNumber(80, label38);
            methodVisitor.visitFrame(Opcodes.F_CHOP, 1, null, 0, null);
            methodVisitor.visitVarInsn(ALOAD, 0);
            methodVisitor.visitFieldInsn(GETFIELD, "java/io/ObjectOutputStream", "enableReplace", "Z");
            Label label45 = new Label();
            methodVisitor.visitJumpInsn(IFEQ, label45);
            Label label46 = new Label();
            methodVisitor.visitLabel(label46);
            methodVisitor.visitLineNumber(81, label46);
            methodVisitor.visitVarInsn(ALOAD, 0);
            methodVisitor.visitVarInsn(ALOAD, 1);
            methodVisitor.visitMethodInsn(INVOKEVIRTUAL,
                    "java/io/ObjectOutputStream",
                    "replaceObject",
                    "(Ljava/lang/Object;)Ljava/lang/Object;",
                    false);
            methodVisitor.visitVarInsn(ASTORE, 8);
            Label label47 = new Label();
            methodVisitor.visitLabel(label47);
            methodVisitor.visitLineNumber(82, label47);
            methodVisitor.visitVarInsn(ALOAD, 8);
            methodVisitor.visitVarInsn(ALOAD, 1);
            Label label48 = new Label();
            methodVisitor.visitJumpInsn(IF_ACMPEQ, label48);
            methodVisitor.visitVarInsn(ALOAD, 8);
            methodVisitor.visitJumpInsn(IFNULL, label48);
            Label label49 = new Label();
            methodVisitor.visitLabel(label49);
            methodVisitor.visitLineNumber(83, label49);
            methodVisitor.visitVarInsn(ALOAD, 8);
            methodVisitor.visitMethodInsn(INVOKEVIRTUAL, "java/lang/Object", "getClass", "()Ljava/lang/Class;", false);
            methodVisitor.visitVarInsn(ASTORE, 6);
            Label label50 = new Label();
            methodVisitor.visitLabel(label50);
            methodVisitor.visitLineNumber(84, label50);
            methodVisitor.visitVarInsn(ALOAD, 6);
            methodVisitor.visitInsn(ICONST_1);
            methodVisitor.visitMethodInsn(INVOKESTATIC,
                    "java/io/ObjectStreamClass",
                    "lookup",
                    "(Ljava/lang/Class;Z)Ljava/io/ObjectStreamClass;",
                    false);
            methodVisitor.visitVarInsn(ASTORE, 7);
            methodVisitor.visitLabel(label48);
            methodVisitor.visitLineNumber(86, label48);
            methodVisitor.visitFrame(Opcodes.F_APPEND, 1, new Object[]{"java/lang/Object"}, 0, null);
            methodVisitor.visitVarInsn(ALOAD, 8);
            methodVisitor.visitVarInsn(ASTORE, 1);
            methodVisitor.visitLabel(label45);
            methodVisitor.visitLineNumber(90, label45);
            methodVisitor.visitFrame(Opcodes.F_CHOP, 1, null, 0, null);
            methodVisitor.visitVarInsn(ALOAD, 1);
            methodVisitor.visitVarInsn(ALOAD, 5);
            methodVisitor.visitJumpInsn(IF_ACMPEQ, label17);
            Label label51 = new Label();
            methodVisitor.visitLabel(label51);
            methodVisitor.visitLineNumber(91, label51);
            methodVisitor.visitVarInsn(ALOAD, 0);
            methodVisitor.visitFieldInsn(GETFIELD,
                    "java/io/ObjectOutputStream",
                    "subs",
                    "Ljava/io/ObjectOutputStream$ReplaceTable;");
            methodVisitor.visitVarInsn(ALOAD, 5);
            methodVisitor.visitVarInsn(ALOAD, 1);
            methodVisitor.visitMethodInsn(INVOKEVIRTUAL,
                    "java/io/ObjectOutputStream$ReplaceTable",
                    "assign",
                    "(Ljava/lang/Object;Ljava/lang/Object;)V",
                    false);
            Label label52 = new Label();
            methodVisitor.visitLabel(label52);
            methodVisitor.visitLineNumber(92, label52);
            methodVisitor.visitVarInsn(ALOAD, 1);
            methodVisitor.visitJumpInsn(IFNONNULL, label11);
            Label label53 = new Label();
            methodVisitor.visitLabel(label53);
            methodVisitor.visitLineNumber(93, label53);
            methodVisitor.visitVarInsn(ALOAD, 0);
            methodVisitor.visitMethodInsn(INVOKESPECIAL, "java/io/ObjectOutputStream", "writeNull", "()V", false);
            methodVisitor.visitLabel(label10);
            methodVisitor.visitLineNumber(125, label10);
            methodVisitor.visitVarInsn(ALOAD, 0);
            methodVisitor.visitInsn(DUP);
            methodVisitor.visitFieldInsn(GETFIELD, "java/io/ObjectOutputStream", "depth", "I");
            methodVisitor.visitInsn(ICONST_1);
            methodVisitor.visitInsn(ISUB);
            methodVisitor.visitFieldInsn(PUTFIELD, "java/io/ObjectOutputStream", "depth", "I");
            Label label54 = new Label();
            methodVisitor.visitLabel(label54);
            methodVisitor.visitLineNumber(126, label54);
            methodVisitor.visitVarInsn(ALOAD, 0);
            methodVisitor.visitFieldInsn(GETFIELD,
                    "java/io/ObjectOutputStream",
                    "bout",
                    "Ljava/io/ObjectOutputStream$BlockDataOutputStream;");
            methodVisitor.visitVarInsn(ILOAD, 3);
            methodVisitor.visitMethodInsn(INVOKEVIRTUAL,
                    "java/io/ObjectOutputStream$BlockDataOutputStream",
                    "setBlockDataMode",
                    "(Z)Z",
                    false);
            methodVisitor.visitInsn(POP);
            Label label55 = new Label();
            methodVisitor.visitLabel(label55);
            methodVisitor.visitLineNumber(94, label55);
            methodVisitor.visitInsn(RETURN);
            methodVisitor.visitLabel(label11);
            methodVisitor.visitLineNumber(95, label11);
            methodVisitor.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
            methodVisitor.visitVarInsn(ILOAD, 2);
            methodVisitor.visitJumpInsn(IFNE, label13);
            methodVisitor.visitVarInsn(ALOAD, 0);
            methodVisitor.visitFieldInsn(GETFIELD,
                    "java/io/ObjectOutputStream",
                    "handles",
                    "Ljava/io/ObjectOutputStream$HandleTable;");
            methodVisitor.visitVarInsn(ALOAD, 1);
            methodVisitor.visitMethodInsn(INVOKEVIRTUAL,
                    "java/io/ObjectOutputStream$HandleTable",
                    "lookup",
                    "(Ljava/lang/Object;)I",
                    false);
            methodVisitor.visitInsn(DUP);
            methodVisitor.visitVarInsn(ISTORE, 4);
            Label label56 = new Label();
            methodVisitor.visitLabel(label56);
            methodVisitor.visitInsn(ICONST_M1);
            methodVisitor.visitJumpInsn(IF_ICMPEQ, label13);
            Label label57 = new Label();
            methodVisitor.visitLabel(label57);
            methodVisitor.visitLineNumber(96, label57);
            methodVisitor.visitVarInsn(ALOAD, 0);
            methodVisitor.visitVarInsn(ILOAD, 4);
            methodVisitor.visitMethodInsn(INVOKESPECIAL, "java/io/ObjectOutputStream", "writeHandle", "(I)V", false);
            methodVisitor.visitLabel(label12);
            methodVisitor.visitLineNumber(125, label12);
            methodVisitor.visitVarInsn(ALOAD, 0);
            methodVisitor.visitInsn(DUP);
            methodVisitor.visitFieldInsn(GETFIELD, "java/io/ObjectOutputStream", "depth", "I");
            methodVisitor.visitInsn(ICONST_1);
            methodVisitor.visitInsn(ISUB);
            methodVisitor.visitFieldInsn(PUTFIELD, "java/io/ObjectOutputStream", "depth", "I");
            Label label58 = new Label();
            methodVisitor.visitLabel(label58);
            methodVisitor.visitLineNumber(126, label58);
            methodVisitor.visitVarInsn(ALOAD, 0);
            methodVisitor.visitFieldInsn(GETFIELD,
                    "java/io/ObjectOutputStream",
                    "bout",
                    "Ljava/io/ObjectOutputStream$BlockDataOutputStream;");
            methodVisitor.visitVarInsn(ILOAD, 3);
            methodVisitor.visitMethodInsn(INVOKEVIRTUAL,
                    "java/io/ObjectOutputStream$BlockDataOutputStream",
                    "setBlockDataMode",
                    "(Z)Z",
                    false);
            methodVisitor.visitInsn(POP);
            Label label59 = new Label();
            methodVisitor.visitLabel(label59);
            methodVisitor.visitLineNumber(97, label59);
            methodVisitor.visitInsn(RETURN);
            methodVisitor.visitLabel(label13);
            methodVisitor.visitLineNumber(98, label13);
            methodVisitor.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
            methodVisitor.visitVarInsn(ALOAD, 1);
            methodVisitor.visitTypeInsn(INSTANCEOF, "java/lang/Class");
            methodVisitor.visitJumpInsn(IFEQ, label15);
            Label label60 = new Label();
            methodVisitor.visitLabel(label60);
            methodVisitor.visitLineNumber(99, label60);
            methodVisitor.visitVarInsn(ALOAD, 0);
            methodVisitor.visitVarInsn(ALOAD, 1);
            methodVisitor.visitTypeInsn(CHECKCAST, "java/lang/Class");
            methodVisitor.visitVarInsn(ILOAD, 2);
            methodVisitor.visitMethodInsn(INVOKESPECIAL,
                    "java/io/ObjectOutputStream",
                    "writeClass",
                    "(Ljava/lang/Class;Z)V",
                    false);
            methodVisitor.visitLabel(label14);
            methodVisitor.visitLineNumber(125, label14);
            methodVisitor.visitVarInsn(ALOAD, 0);
            methodVisitor.visitInsn(DUP);
            methodVisitor.visitFieldInsn(GETFIELD, "java/io/ObjectOutputStream", "depth", "I");
            methodVisitor.visitInsn(ICONST_1);
            methodVisitor.visitInsn(ISUB);
            methodVisitor.visitFieldInsn(PUTFIELD, "java/io/ObjectOutputStream", "depth", "I");
            Label label61 = new Label();
            methodVisitor.visitLabel(label61);
            methodVisitor.visitLineNumber(126, label61);
            methodVisitor.visitVarInsn(ALOAD, 0);
            methodVisitor.visitFieldInsn(GETFIELD,
                    "java/io/ObjectOutputStream",
                    "bout",
                    "Ljava/io/ObjectOutputStream$BlockDataOutputStream;");
            methodVisitor.visitVarInsn(ILOAD, 3);
            methodVisitor.visitMethodInsn(INVOKEVIRTUAL,
                    "java/io/ObjectOutputStream$BlockDataOutputStream",
                    "setBlockDataMode",
                    "(Z)Z",
                    false);
            methodVisitor.visitInsn(POP);
            Label label62 = new Label();
            methodVisitor.visitLabel(label62);
            methodVisitor.visitLineNumber(100, label62);
            methodVisitor.visitInsn(RETURN);
            methodVisitor.visitLabel(label15);
            methodVisitor.visitLineNumber(101, label15);
            methodVisitor.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
            methodVisitor.visitVarInsn(ALOAD, 1);
            methodVisitor.visitTypeInsn(INSTANCEOF, "java/io/ObjectStreamClass");
            methodVisitor.visitJumpInsn(IFEQ, label17);
            Label label63 = new Label();
            methodVisitor.visitLabel(label63);
            methodVisitor.visitLineNumber(102, label63);
            methodVisitor.visitVarInsn(ALOAD, 0);
            methodVisitor.visitVarInsn(ALOAD, 1);
            methodVisitor.visitTypeInsn(CHECKCAST, "java/io/ObjectStreamClass");
            methodVisitor.visitVarInsn(ILOAD, 2);
            methodVisitor.visitMethodInsn(INVOKESPECIAL,
                    "java/io/ObjectOutputStream",
                    "writeClassDesc",
                    "(Ljava/io/ObjectStreamClass;Z)V",
                    false);
            methodVisitor.visitLabel(label16);
            methodVisitor.visitLineNumber(125, label16);
            methodVisitor.visitVarInsn(ALOAD, 0);
            methodVisitor.visitInsn(DUP);
            methodVisitor.visitFieldInsn(GETFIELD, "java/io/ObjectOutputStream", "depth", "I");
            methodVisitor.visitInsn(ICONST_1);
            methodVisitor.visitInsn(ISUB);
            methodVisitor.visitFieldInsn(PUTFIELD, "java/io/ObjectOutputStream", "depth", "I");
            Label label64 = new Label();
            methodVisitor.visitLabel(label64);
            methodVisitor.visitLineNumber(126, label64);
            methodVisitor.visitVarInsn(ALOAD, 0);
            methodVisitor.visitFieldInsn(GETFIELD,
                    "java/io/ObjectOutputStream",
                    "bout",
                    "Ljava/io/ObjectOutputStream$BlockDataOutputStream;");
            methodVisitor.visitVarInsn(ILOAD, 3);
            methodVisitor.visitMethodInsn(INVOKEVIRTUAL,
                    "java/io/ObjectOutputStream$BlockDataOutputStream",
                    "setBlockDataMode",
                    "(Z)Z",
                    false);
            methodVisitor.visitInsn(POP);
            Label label65 = new Label();
            methodVisitor.visitLabel(label65);
            methodVisitor.visitLineNumber(103, label65);
            methodVisitor.visitInsn(RETURN);
            methodVisitor.visitLabel(label17);
            methodVisitor.visitLineNumber(108, label17);
            methodVisitor.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
            methodVisitor.visitVarInsn(ALOAD, 1);
            methodVisitor.visitTypeInsn(INSTANCEOF, "java/lang/String");
            Label label66 = new Label();
            methodVisitor.visitJumpInsn(IFEQ, label66);
            Label label67 = new Label();
            methodVisitor.visitLabel(label67);
            methodVisitor.visitLineNumber(109, label67);
            methodVisitor.visitVarInsn(ALOAD, 0);
            methodVisitor.visitVarInsn(ALOAD, 1);
            methodVisitor.visitTypeInsn(CHECKCAST, "java/lang/String");
            methodVisitor.visitVarInsn(ILOAD, 2);
            methodVisitor.visitMethodInsn(INVOKESPECIAL,
                    "java/io/ObjectOutputStream",
                    "writeString",
                    "(Ljava/lang/String;Z)V",
                    false);
            methodVisitor.visitJumpInsn(GOTO, label18);
            methodVisitor.visitLabel(label66);
            methodVisitor.visitLineNumber(110, label66);
            methodVisitor.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
            methodVisitor.visitVarInsn(ALOAD, 6);
            methodVisitor.visitMethodInsn(INVOKEVIRTUAL, "java/lang/Class", "isArray", "()Z", false);
            Label label68 = new Label();
            methodVisitor.visitJumpInsn(IFEQ, label68);
            Label label69 = new Label();
            methodVisitor.visitLabel(label69);
            methodVisitor.visitLineNumber(111, label69);
            methodVisitor.visitVarInsn(ALOAD, 0);
            methodVisitor.visitVarInsn(ALOAD, 1);
            methodVisitor.visitVarInsn(ALOAD, 7);
            methodVisitor.visitVarInsn(ILOAD, 2);
            methodVisitor.visitMethodInsn(INVOKESPECIAL,
                    "java/io/ObjectOutputStream",
                    "writeArray",
                    "(Ljava/lang/Object;Ljava/io/ObjectStreamClass;Z)V",
                    false);
            methodVisitor.visitJumpInsn(GOTO, label18);
            methodVisitor.visitLabel(label68);
            methodVisitor.visitLineNumber(112, label68);
            methodVisitor.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
            methodVisitor.visitVarInsn(ALOAD, 1);
            methodVisitor.visitTypeInsn(INSTANCEOF, "java/lang/Enum");
            Label label70 = new Label();
            methodVisitor.visitJumpInsn(IFEQ, label70);
            Label label71 = new Label();
            methodVisitor.visitLabel(label71);
            methodVisitor.visitLineNumber(113, label71);
            methodVisitor.visitVarInsn(ALOAD, 0);
            methodVisitor.visitVarInsn(ALOAD, 1);
            methodVisitor.visitTypeInsn(CHECKCAST, "java/lang/Enum");
            methodVisitor.visitVarInsn(ALOAD, 7);
            methodVisitor.visitVarInsn(ILOAD, 2);
            methodVisitor.visitMethodInsn(INVOKESPECIAL,
                    "java/io/ObjectOutputStream",
                    "writeEnum",
                    "(Ljava/lang/Enum;Ljava/io/ObjectStreamClass;Z)V",
                    false);
            methodVisitor.visitJumpInsn(GOTO, label18);
            methodVisitor.visitLabel(label70);
            methodVisitor.visitLineNumber(114, label70);
            methodVisitor.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
            methodVisitor.visitVarInsn(ALOAD, 1);
            methodVisitor.visitTypeInsn(INSTANCEOF, "java/io/Serializable");
            Label label72 = new Label();
            methodVisitor.visitJumpInsn(IFNE, label72);
            methodVisitor.visitVarInsn(ALOAD, 0);
            methodVisitor.visitFieldInsn(GETFIELD, "java/io/ObjectOutputStream", "excludeSet", "Ljava/util/Set;");
            methodVisitor.visitVarInsn(ALOAD, 1);
            Label label73 = new Label();
            methodVisitor.visitLabel(label73);
            methodVisitor.visitLineNumber(115, label73);
            methodVisitor.visitMethodInsn(INVOKEVIRTUAL, "java/lang/Object", "getClass", "()Ljava/lang/Class;", false);
            methodVisitor.visitMethodInsn(INVOKEVIRTUAL,
                    "java/lang/Class",
                    "getCanonicalName",
                    "()Ljava/lang/String;",
                    false);
            methodVisitor.visitMethodInsn(INVOKEINTERFACE, "java/util/Set", "contains", "(Ljava/lang/Object;)Z", true);
            Label label74 = new Label();
            methodVisitor.visitJumpInsn(IFEQ, label74);
            methodVisitor.visitLabel(label72);
            methodVisitor.visitLineNumber(116, label72);
            methodVisitor.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
            methodVisitor.visitVarInsn(ALOAD, 0);
            methodVisitor.visitVarInsn(ALOAD, 1);
            methodVisitor.visitVarInsn(ALOAD, 7);
            methodVisitor.visitVarInsn(ILOAD, 2);
            methodVisitor.visitMethodInsn(INVOKESPECIAL,
                    "java/io/ObjectOutputStream",
                    "writeOrdinaryObject",
                    "(Ljava/lang/Object;Ljava/io/ObjectStreamClass;Z)V",
                    false);
            methodVisitor.visitJumpInsn(GOTO, label18);
            methodVisitor.visitLabel(label74);
            methodVisitor.visitLineNumber(118, label74);
            methodVisitor.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
            methodVisitor.visitFieldInsn(GETSTATIC, "java/io/ObjectOutputStream", "extendedDebugInfo", "Z");
            Label label75 = new Label();
            methodVisitor.visitJumpInsn(IFEQ, label75);
            Label label76 = new Label();
            methodVisitor.visitLabel(label76);
            methodVisitor.visitLineNumber(119, label76);
            methodVisitor.visitTypeInsn(NEW, "java/io/NotSerializableException");
            methodVisitor.visitInsn(DUP);
            methodVisitor.visitTypeInsn(NEW, "java/lang/StringBuilder");
            methodVisitor.visitInsn(DUP);
            methodVisitor.visitMethodInsn(INVOKESPECIAL, "java/lang/StringBuilder", "<init>", "()V", false);
            methodVisitor.visitVarInsn(ALOAD, 6);
            methodVisitor.visitMethodInsn(INVOKEVIRTUAL, "java/lang/Class", "getName", "()Ljava/lang/String;", false);
            methodVisitor.visitMethodInsn(INVOKEVIRTUAL,
                    "java/lang/StringBuilder",
                    "append",
                    "(Ljava/lang/String;)Ljava/lang/StringBuilder;",
                    false);
            methodVisitor.visitLdcInsn("\n");
            methodVisitor.visitMethodInsn(INVOKEVIRTUAL,
                    "java/lang/StringBuilder",
                    "append",
                    "(Ljava/lang/String;)Ljava/lang/StringBuilder;",
                    false);
            methodVisitor.visitVarInsn(ALOAD, 0);
            methodVisitor.visitFieldInsn(GETFIELD,
                    "java/io/ObjectOutputStream",
                    "debugInfoStack",
                    "Ljava/io/ObjectOutputStream$DebugTraceInfoStack;");
            methodVisitor.visitMethodInsn(INVOKEVIRTUAL, "java/lang/Object", "toString", "()Ljava/lang/String;", false);
            methodVisitor.visitMethodInsn(INVOKEVIRTUAL,
                    "java/lang/StringBuilder",
                    "append",
                    "(Ljava/lang/String;)Ljava/lang/StringBuilder;",
                    false);
            methodVisitor.visitMethodInsn(INVOKEVIRTUAL,
                    "java/lang/StringBuilder",
                    "toString",
                    "()Ljava/lang/String;",
                    false);
            methodVisitor.visitMethodInsn(INVOKESPECIAL,
                    "java/io/NotSerializableException",
                    "<init>",
                    "(Ljava/lang/String;)V",
                    false);
            methodVisitor.visitInsn(ATHROW);
            methodVisitor.visitLabel(label75);
            methodVisitor.visitLineNumber(121, label75);
            methodVisitor.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
            methodVisitor.visitTypeInsn(NEW, "java/io/NotSerializableException");
            methodVisitor.visitInsn(DUP);
            methodVisitor.visitVarInsn(ALOAD, 6);
            methodVisitor.visitMethodInsn(INVOKEVIRTUAL, "java/lang/Class", "getName", "()Ljava/lang/String;", false);
            methodVisitor.visitMethodInsn(INVOKESPECIAL,
                    "java/io/NotSerializableException",
                    "<init>",
                    "(Ljava/lang/String;)V",
                    false);
            methodVisitor.visitInsn(ATHROW);
            methodVisitor.visitLabel(label18);
            methodVisitor.visitLineNumber(125, label18);
            methodVisitor.visitFrame(Opcodes.F_FULL,
                    4,
                    new Object[]{"java/io/ObjectOutputStream", "java/lang/Object", Opcodes.INTEGER, Opcodes.INTEGER},
                    0,
                    new Object[]{});
            methodVisitor.visitVarInsn(ALOAD, 0);
            methodVisitor.visitInsn(DUP);
            methodVisitor.visitFieldInsn(GETFIELD, "java/io/ObjectOutputStream", "depth", "I");
            methodVisitor.visitInsn(ICONST_1);
            methodVisitor.visitInsn(ISUB);
            methodVisitor.visitFieldInsn(PUTFIELD, "java/io/ObjectOutputStream", "depth", "I");
            Label label77 = new Label();
            methodVisitor.visitLabel(label77);
            methodVisitor.visitLineNumber(126, label77);
            methodVisitor.visitVarInsn(ALOAD, 0);
            methodVisitor.visitFieldInsn(GETFIELD,
                    "java/io/ObjectOutputStream",
                    "bout",
                    "Ljava/io/ObjectOutputStream$BlockDataOutputStream;");
            methodVisitor.visitVarInsn(ILOAD, 3);
            methodVisitor.visitMethodInsn(INVOKEVIRTUAL,
                    "java/io/ObjectOutputStream$BlockDataOutputStream",
                    "setBlockDataMode",
                    "(Z)Z",
                    false);
            methodVisitor.visitInsn(POP);
            Label label78 = new Label();
            methodVisitor.visitLabel(label78);
            methodVisitor.visitLineNumber(127, label78);
            Label label79 = new Label();
            methodVisitor.visitJumpInsn(GOTO, label79);
            methodVisitor.visitLabel(label2);
            methodVisitor.visitLineNumber(125, label2);
            methodVisitor.visitFrame(Opcodes.F_SAME1, 0, null, 1, new Object[]{"java/lang/Throwable"});
            methodVisitor.visitVarInsn(ASTORE, 9);
            methodVisitor.visitLabel(label19);
            methodVisitor.visitVarInsn(ALOAD, 0);
            methodVisitor.visitInsn(DUP);
            methodVisitor.visitFieldInsn(GETFIELD, "java/io/ObjectOutputStream", "depth", "I");
            methodVisitor.visitInsn(ICONST_1);
            methodVisitor.visitInsn(ISUB);
            methodVisitor.visitFieldInsn(PUTFIELD, "java/io/ObjectOutputStream", "depth", "I");
            Label label80 = new Label();
            methodVisitor.visitLabel(label80);
            methodVisitor.visitLineNumber(126, label80);
            methodVisitor.visitVarInsn(ALOAD, 0);
            methodVisitor.visitFieldInsn(GETFIELD,
                    "java/io/ObjectOutputStream",
                    "bout",
                    "Ljava/io/ObjectOutputStream$BlockDataOutputStream;");
            methodVisitor.visitVarInsn(ILOAD, 3);
            methodVisitor.visitMethodInsn(INVOKEVIRTUAL,
                    "java/io/ObjectOutputStream$BlockDataOutputStream",
                    "setBlockDataMode",
                    "(Z)Z",
                    false);
            methodVisitor.visitInsn(POP);
            methodVisitor.visitVarInsn(ALOAD, 9);
            methodVisitor.visitInsn(ATHROW);
            methodVisitor.visitLabel(label79);
            methodVisitor.visitLineNumber(128, label79);
            methodVisitor.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
            methodVisitor.visitInsn(RETURN);
            Label label81 = new Label();
            methodVisitor.visitLabel(label81);
            methodVisitor.visitLocalVariable("h", "I", null, label25, label5, 4);
            methodVisitor.visitLocalVariable("repCl", "Ljava/lang/Class;", "Ljava/lang/Class<*>;", label41, label44, 8);
            methodVisitor.visitLocalVariable("rep", "Ljava/lang/Object;", null, label47, label45, 8);
            methodVisitor.visitLocalVariable("h", "I", null, label56, label13, 4);
            methodVisitor.visitLocalVariable("orig", "Ljava/lang/Object;", null, label35, label18, 5);
            methodVisitor.visitLocalVariable("cl", "Ljava/lang/Class;", "Ljava/lang/Class<*>;", label36, label18, 6);
            methodVisitor.visitLocalVariable("desc", "Ljava/io/ObjectStreamClass;", null, label37, label18, 7);
            methodVisitor.visitLocalVariable("this", "Ljava/io/ObjectOutputStream;", null, label20, label81, 0);
            methodVisitor.visitLocalVariable("obj", "Ljava/lang/Object;", null, label20, label81, 1);
            methodVisitor.visitLocalVariable("unshared", "Z", null, label20, label81, 2);
            methodVisitor.visitLocalVariable("oldMode", "Z", null, label21, label81, 3);
            methodVisitor.visitMaxs(4, 10);
            methodVisitor.visitEnd();
        }
    }

    private static byte[] modClass(String className, byte[] classfileBuffer, Consumer<Param> function, boolean dump) {
        ClassWriter classWriter = new ClassWriter(ClassWriter.COMPUTE_FRAMES);
        ClassReader classReader = new ClassReader(classfileBuffer);
        ClassAdapterX classAdapterX = new ClassAdapterX(ASM5, classWriter, HandleAgent.getCanonicalName(className));
        classReader.accept(classAdapterX, ClassReader.EXPAND_FRAMES);
        FieldVisitor fieldVisitor = null;
        MethodVisitor methodVisitor = null;
        function.accept(new Param(fieldVisitor, methodVisitor, classWriter));
        classWriter.visitEnd();
        byte[] b = classWriter.toByteArray();
        if (dump) {
            File file = new File("D:\\z_wd\\handle_agent\\dbg\\" + className);
            try {
                FileOutputStream fileOutputStream = new FileOutputStream(file);
                fileOutputStream.write(b);
                fileOutputStream.flush();
                fileOutputStream.close();
            } catch (FileNotFoundException e) {
                throw new RuntimeException(e);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
        return b;
    }


    static final class Param {
        FieldVisitor fieldVisitor;
        MethodVisitor methodVisitor;
        ClassWriter classWriter;

        public Param(FieldVisitor fieldVisitor, MethodVisitor methodVisitor, ClassWriter classWriter) {
            this.fieldVisitor = fieldVisitor;
            this.methodVisitor = methodVisitor;
            this.classWriter = classWriter;
        }
    }

    static class ClassAdapterX extends ClassVisitor {

        static final Map<String, Set<String>> rewriteMap = new HashMap() {{
            put("java.io.ObjectOutputStream", new HashSet() {{
                add("writeObject0");
                add("<init>");
            }});
            put("java.io.ObjectInputStream", new HashSet() {{
                add("readOrdinaryObject");
            }});
        }};
        String className;

        public ClassAdapterX(int i, ClassVisitor classVisitor, String className) {
            super(i, classVisitor);
            this.className = className;
        }

        @Override
        public MethodVisitor visitMethod(int i, String s, String s1, String s2, String[] strings) {
            Set<String> methods = rewriteMap.get(className);
            if (methods != null && methods.contains(s)) {
                return null;
            }
            return super.visitMethod(i, s, s1, s2, strings);
        }
    }
}
