import jdk.internal.org.objectweb.asm.*;

import java.io.*;
import java.lang.instrument.ClassFileTransformer;
import java.lang.instrument.IllegalClassFormatException;
import java.lang.instrument.Instrumentation;
import java.security.ProtectionDomain;

import static jdk.internal.org.objectweb.asm.Opcodes.*;

/**
 * -javaagent:plugin-agent.jar
 */
public class HandleAgent {

	public static void premain(String agentArgs, Instrumentation ins) {
		System.out.println("--------------javaagent-----------------");
		ins.addTransformer(new ClassFileTransformer() {
			@Override
			public byte[] transform(ClassLoader loader, String className, Class<?> classBeingRedefined,
									ProtectionDomain protectionDomain, byte[] classfileBuffer)
					throws IllegalClassFormatException {
				try {
					if (className != null) {
						return CodeChanger.handle(className, classfileBuffer, classBeingRedefined);
					}
					return classfileBuffer;
				} catch (IOException e) {
					throw new RuntimeException(e);
				}
			}
		});
	}

	public static boolean startWith(String className, String aName) {
		return getCanonicalName(className).startsWith(aName);
	}

	public static String getCanonicalName(String className) {
		return className.replace('/', '.');
	}

	public static boolean isEquals(String className, String aName) {
		return getCanonicalName(className).equals(aName);
	}

	public static void main(String[] args) throws IOException {
		byte[] b = new byte[512];
		ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
		FileInputStream fileInputStream =
				new FileInputStream("D:\\z_wd\\handle_agent\\target\\classes\\HandleAgent.class");

		int read;
		while ((read = fileInputStream.read(b)) != -1) {
			byteArrayOutputStream.write(b, 0, read);
		}
		ClassWriter classWriter = new ClassWriter(ClassWriter.COMPUTE_FRAMES);
		ClassReader classReader = new ClassReader(byteArrayOutputStream.toByteArray());
		ClassAdapterT cr = new ClassAdapterT(ASM5, classWriter);
		classReader.accept(cr, ClassReader.EXPAND_FRAMES);
		MethodVisitor methodVisitor;

		{
			methodVisitor = classWriter.visitMethod(ACC_PRIVATE | ACC_STATIC,
					"isEquals",
					"(Ljava/lang/String;Ljava/lang/String;)Z",
					null,
					null);
			methodVisitor.visitCode();
			Label label0 = new Label();
			methodVisitor.visitLabel(label0);
			methodVisitor.visitLineNumber(983, label0);
			methodVisitor.visitVarInsn(ALOAD, 0);
			methodVisitor.visitIntInsn(BIPUSH, 47);
			methodVisitor.visitIntInsn(BIPUSH, 46);
			methodVisitor.visitMethodInsn(INVOKEVIRTUAL,
					"java/lang/String",
					"replace",
					"(CC)Ljava/lang/String;",
					false);
			methodVisitor.visitVarInsn(ALOAD, 1);
			methodVisitor.visitMethodInsn(INVOKEVIRTUAL, "java/lang/String", "equals", "(Ljava/lang/Object;)Z", false);
			methodVisitor.visitInsn(IRETURN);
			Label label1 = new Label();
			methodVisitor.visitLabel(label1);
			methodVisitor.visitLocalVariable("className", "Ljava/lang/String;", null, label0, label1, 0);
			methodVisitor.visitLocalVariable("anObject", "Ljava/lang/String;", null, label0, label1, 1);
			methodVisitor.visitMaxs(3, 2);
			methodVisitor.visitEnd();
		}
		classWriter.visitEnd();
		try {
			new FileOutputStream(new File("D:\\z_wd\\handle_agent\\target\\test2.class")).write(classWriter.toByteArray());
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	static class ClassAdapterT extends ClassVisitor {


		public ClassAdapterT(int i, ClassVisitor classVisitor) {
			super(i, classVisitor);
		}

		@Override
		public MethodVisitor visitMethod(int i, String s, String s1, String s2, String[] strings) {
			if (s.equals("isEquals")) {
				return null;
			}
			return super.visitMethod(i, s, s1, s2, strings);
		}
	}
}
